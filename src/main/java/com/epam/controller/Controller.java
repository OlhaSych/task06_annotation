package com.epam.controller;

import com.epam.model.MyAnnotation;
import com.epam.model.MyClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Controller {
    private static final Logger logger = LogManager.getLogger();
    private static final MyClass myClass = new MyClass("NewClass", 1, true);
    private static final Class cls = myClass.getClass();

    public static void showFields() {
        Field[] declaredFields = cls.getDeclaredFields();
        for (Field field : declaredFields) {
            logger.info(field);
        }
    }

    public static void printAnnotation() {
        logger.info("PRINT ANNOTATION INFORMATION:");
        Field[] declaredFields = cls.getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            MyAnnotation myAnnotation = field.getAnnotation(MyAnnotation.class);
            if (myAnnotation != null) {
                logger.info("name of field: "+field.getName() + ", annotation values :"
                        + myAnnotation.label() + " " + myAnnotation.exist());
            } else {
                logger.info("name of field: "+field.getName() + ", there is no annotation");
            }
        }
    }

    public static int countAmountOfUse(int number) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        Method method = cls.getDeclaredMethod("countAmountOfUse", int.class);
        return (int) method.invoke(myClass, number);
    }

    public static String showPrediction(String title, boolean available) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        Method method = cls.getDeclaredMethod("showPrediction", String.class, boolean.class);
        return (String) method.invoke(myClass, title, available);
    }

    public static String[] changeTitles(String title, int... numbers) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        Method method = cls.getDeclaredMethod("changeTitles", String.class, int[].class);
        return (String[]) method.invoke(myClass, title, numbers);
    }
}
