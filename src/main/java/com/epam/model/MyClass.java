package com.epam.model;

public class MyClass {
    @MyAnnotation(label = "win")
    private String title;
    private int amountOfUse;
    @MyAnnotation(exist = false)
    private boolean available;

    public MyClass(String title, int amountOfUse, boolean available) {
        this.title = title;
        this.amountOfUse = amountOfUse;
        this.available = available;
    }

    public int countAmountOfUse(int number) {
        return amountOfUse += number;
    }

    public String showPrediction(String title, boolean available) {
        String str = "everything will be fine";
        if (available) {
            str = title;
        }
        return str;
    }

    public String[] changeTitles(String title, int... numbers) {
        String[] array = new String[numbers.length];
        for (int i = 0; i < array.length; i++) {
            for (int number : numbers
            ) {
                array[i] = title +number+i;
            }
        }
        return array;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAmountOfUse() {
        return amountOfUse;
    }

    public void setAmountOfUse(int amountOfUse) {
        this.amountOfUse = amountOfUse;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "MyClass{" +
                "title='" + title + '\'' +
                ", amountOfUse=" + amountOfUse +
                ", available=" + available +
                '}';
    }
}
