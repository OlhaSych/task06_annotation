package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class Information {
    private static final Logger logger = LogManager.getLogger();

    public static void getInformation(Object object) {
        Class cls = object.getClass();
        logger.info("name of class :" + cls.getName());
        logger.info("name of super class :" + cls.getSuperclass().getName());
        for (Class inter : cls.getInterfaces()
        ) {
            logger.info("name of interface :" + inter.getName());
        }
        for (Field field : cls.getDeclaredFields()
        ) {
            logger.info("name of field :" + field.getName() + ", type:" + field.getType());
        }
        for (Constructor constructor : cls.getDeclaredConstructors()) {
            logger.info("name of constructor :" + constructor.getModifiers() +
                    ", name: " + constructor.getName());
        }
        for (Method method : cls.getDeclaredMethods()
        ) {
            logger.info("name of method :" + method.getName());
            for (Parameter parameter : method.getParameters()
            ) {
                logger.info("name of parameter :" + parameter.getName()
                        + ", type: " + parameter.getType());
            }
        }
    }
}
