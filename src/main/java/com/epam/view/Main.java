package com.epam.view;

import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;

public class Main {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        logger.info("INFORMATION ABOUT FIELDS:");
        Controller.showFields();
        Controller.printAnnotation();
        try {
            logger.info("INVOKE SOME METHODS:");
            int amount = Controller.countAmountOfUse(2);
            logger.info(amount);
            String str = Controller.showPrediction("Be positive!", true);
            logger.info(str);
            String[] arrays = Controller.changeTitles("Movie", 1, 2, 3);
            for (String s : arrays
            ) {
                logger.info(s);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
